# install and set locales to en_US.UTF-8

install_locales () {
    # source: https://www.jaredwolff.com/raspberry-pi-setting-your-locale/
    sudo perl -pi -e 's/# en_US.UTF-8 UTF-8/en_US.UTF-8 UTF-8/g' /etc/locale.gen
    sudo locale-gen en_US.UTF-8
    sudo update-locale en_US.UTF-8
}

install_locales
