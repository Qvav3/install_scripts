# Initialize Raspberrypi

- Download Raspberry Pi OS (64-bit) Lite from `https://www.raspberrypi.com/software/operating-systems/`
- Write img to sd card


## Configuring a User

- source: `https://www.raspberrypi.com/documentation/computers/configuration.html#configuring-a-user`
- Write `ssh` file to bootfs: `touch ssh`
- Create encrypted password with `openssl passwd -6`
- write `userconf.txt` file with following content to bootfs: `{username}:{encrypted_password}`


## Disable Password Authentication

- open file `/etc/ssh/sshd_config`
- change `PasswordAuthentication yes` to `PasswordAuthentication no`

## Configuration

- run scripts in folder

