# Install Docker
# Last checked 04-Sep-23

install_docker () {
    # source: https://docs.docker.com/engine/install/debian/
    # Update the apt package index and install packages (all packages should already be installed)
    sudo apt-get update
    sudo apt-get install ca-certificates curl gnupg

    # Add Docker's official GPG key:
    sudo apt-get update
    sudo apt-get install ca-certificates curl gnupg
    sudo install -m 0755 -d /etc/apt/keyrings
    curl -fsSL https://download.docker.com/linux/debian/gpg | sudo gpg --dearmor -o /etc/apt/keyrings/docker.gpg
    sudo chmod a+r /etc/apt/keyrings/docker.gpg

    # Add the repository to Apt sources:
    echo \
      "deb [arch="$(dpkg --print-architecture)" signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/debian \
      "$(. /etc/os-release && echo "$VERSION_CODENAME")" stable" | \
      sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
    sudo apt-get update

    # install docker engine
    sudo apt-get install docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin
    # Verify that the Docker Engine installation is successful by running the hello-world image
    sudo docker run hello-world
    # This command downloads a test image and runs it in a container.
    # When the container runs, it prints a confirmation message and exits
}

manage_docker_as_non_root_user () {
    # source: https://docs.docker.com/engine/install/linux-postinstall/
    sudo groupadd docker
    sudo usermod -aG docker $USER
    newgrp docker
    docker run hello-world
}

configure_docker_to_start_on_boot () {
    # source: https://docs.docker.com/engine/install/linux-postinstall/
    sudo systemctl enable docker.service
    sudo systemctl enable containerd.service
}

install_docker
manage_docker_as_non_root_user
configure_docker_to_start_on_boot

